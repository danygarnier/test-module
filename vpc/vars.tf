variable "region" {
  description = "Region"
}

variable "allowed_account_ids" {
  description = "List of allowed account ids"
  type        = list(string)
}

variable env {
  description = "Environment [dev,pprd,prd]"
}

variable app_name {
  description = "Application Name"
}

variable "cidr" {
  description = "Vpc cidr"
}

variable "azs" {
  description = "vpc avalability zones"
  type        = list(string)
}

variable "public_subnets" {
  description = "Public subnets"
  type        = list(string)
}

variable "private_subnets" {
  description = "Private subnets"
  type        = list(string)
}

variable "enable_dns_support" {
  description = "Enable dns support"
  type        = bool
}

variable "enable_dns_hostnames" {
  description = "Enable dns  hostname"
}

variable "enable_nat_gateway" {
  description = "Enable nat gateway"
}

