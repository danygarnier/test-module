provider "aws" {
  region              = var.region
  allowed_account_ids = var.allowed_account_ids
  version             = ">= 0.12.0"
}

terraform {
  backend "s3" {}
}

locals {
  common_tags = {
    Builder     = "Terraform"
    AppName     = var.app_name
    Environment = var.env
  }
}

module "vpc" {

  # Dont use Registry URL but GitHub since it cannot be operate by terragrunt

  source  = "terraform-aws-modules/vpc/aws"
  version = "2.6.0"

  name                               = "${var.app_name}-${var.env}"
  cidr                               = var.cidr
  azs                                = var.azs
  public_subnets                     = var.public_subnets
  private_subnets                    = var.private_subnets
  enable_dns_hostnames               = var.enable_dns_hostnames
  enable_dns_support                 = var.enable_dns_support
  enable_nat_gateway                 = var.enable_nat_gateway
  single_nat_gateway                 = true
  propagate_public_route_tables_vgw  = true
  propagate_private_route_tables_vgw = true

  tags = local.common_tags
}
